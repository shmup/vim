vim stuff using [pathogen](https://github.com/tpope/vim-pathogen)

```
git clone https://gitlab.com/shmup/vim.git ~/.vim
cd ~/.vim && git submodule init && git submodule update
mkdir -p ~/.vim/junk ~/.vim/junk/backup ~/.vim/junk/swp ~/.vim/junk/undo ~/.vim/junk/view
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```
